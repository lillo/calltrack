type 'a t = 'a list

let empty_set = ([]: 'a t)

let add_point (p, set) = 
  if (List.mem p set) then
    set
  else
    p :: set

let rec combine_set (set1, set2) = 
  match set1 with
      [] -> set2
    | h::t -> combine_set (t, add_point(h,set2))

let to_list set = set
let from_list l = l
