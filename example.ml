open Syntax

(*
 Factorial

  let rec fact = fun[fact_point] n ->
    if (iszero n) then
      1
    else
      n * (fact (n - 1))

*)
let fact = 
  Rec("fact",
      Fun("fact_point","n",
	  IfThenElse(Iszero(Ide("n")), 
		     Eint(1), 
		     Times(Ide("n"), Apply(Ide("fact"),Minus(Ide("n"),Eint(1)))))
     ))

(*
  fact 5
*)
let fact5 = 
  Let("fact", fact, Apply(Ide("fact"), Eint(5)))

(*
  exponential

  let rec exp = fun[exp1] b -> fun[exp2] n ->
    if (iszero n) then
      1
    else
      b * (exp b (n - 1))
*)

let exp = 
  Rec("exp",
      Fun("exp1", "b",
	  Fun("exp2", "n",
	      IfThenElse(Iszero(Ide("n")),
			 Eint(1), 
			 Times(Ide("b"), 
			       Apply(Apply(Ide("exp"), Ide("b")),Minus(Ide("n"), Eint(1))))
	      )
	     )
	 )
     )

(*
  exp 2 3
*)

let exp23 = 
  Let("exp", exp, Apply(Apply(Ide("exp"),Eint(2)), Eint(3)))


(*
  let a = fun[a_point] x -> true in
  let b = fun[b_point] x -> false in
    (a 1) or (b 1)
*)

let appa = 
  Let("a",Fun("a_point","x", Ebool(true)),
      Let("b",Fun("b_point","x", Ebool(false)),
	  Or(Apply(Ide("a"),Eint(1)),Apply(Ide("b"),Eint(1)))
	 ))
