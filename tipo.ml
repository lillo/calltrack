type tipo = 
    TypeVar of string
  | AnnVar of string
  | Symb of string * tipo list

type subst = (tipo * tipo) list

let single_subst (var,term) = ([var,term] : subst)
let empty_subst = ([]:subst)

let rec tipo_printer pff t = 
  match t with 
      TypeVar(var) 
    | AnnVar(var) -> 
	Format.fprintf pff "@[<2>_%s_@]" var
    | Symb(name, subterms) ->
	if (List.length subterms) > 0 then
	  Format.fprintf pff "@[<2>%s(%a%a)@]" name 
	    tipo_printer (List.hd subterms) 
	    l_tipo_printer (List.tl subterms)
	else
	  Format.fprintf pff "@[<2>%s@]" name 

and l_tipo_printer pff term_list = 
  match term_list with
      [] -> ()
    | h::t -> Format.fprintf pff "@[<2>,@ %a%a@]" tipo_printer h l_tipo_printer t

let rec apply_subst subst term = 
  match term with
      TypeVar(_)
    | AnnVar(_) -> 
	(try
	   List.assoc term subst
	 with Not_found -> term
	)
    | Symb(name, subterms) ->
	Symb(name, List.map (apply_subst subst) subterms)

let compose (subst1:subst) subst2 =
  let rec f_aux s1 pr =
    match s1 with
        [] -> pr
      | (x,t)::s ->
          let n = apply_subst subst2 t in
            f_aux s ((x,n)::pr)
  and diff r s2 =
    match s2 with
        [] -> r
      | (x,t)::s ->
          let n_r =
            if (List.mem_assoc x r) then
              r
            else
              (x,t)::r
          in
            diff n_r s
  in
    diff (f_aux subst1 []) subst2

let rec occur_check var term = 
    match term with
	TypeVar(v)
      | AnnVar(v) -> v = var
      | Symb(name, subterms) ->
	  List.fold_left (fun b t -> b || (occur_check var t)) false subterms

let rec unify term1 term2 = 
  match (term1,term2) with
      (TypeVar(v1),TypeVar(v2))
    | (AnnVar(v1), AnnVar(v2)) ->
	if v1 = v2 then empty_subst else single_subst (term1,term2)
    | (AnnVar(_),_)
    | (_,AnnVar(_)) ->
	failwith "No unification - annotation variable"
    | (TypeVar(v),t)
    | (t,TypeVar(v)) ->
	if occur_check v t then
	  failwith "No unification - occur check"
	else
	  single_subst ((TypeVar(v)),t)
    | (Symb(name1,subterms1),Symb(name2, subterms2)) 
	when name1 = name2 ->
	if (List.length subterms1) = (List.length subterms2) then
	  l_unify subterms1 subterms2
	else
	  failwith ("No unification:" ^ name1 ^ " different arity")
    | _ -> failwith ("No unification: different constructor")

and l_unify tlist1 tlist2 = 
  match (tlist1,tlist2) with
      ([],[]) -> empty_subst
    | ((h1::t1),(h2::t2)) ->
	let s = unify h1 h2 in
        let n_t1 = List.map (apply_subst s) t1
        and n_t2 = List.map (apply_subst s) t2
        in
          compose s (l_unify n_t1 n_t2)
    | _ -> failwith "No unification - different arity"

let apply_subst_eqlist sub term_list =
  let apply_subst_eq (e1,e2) =
    (apply_subst sub e1, apply_subst sub e2)
  in
  List.map apply_subst_eq term_list

let rec unify_list equation_list =
  match equation_list with
      [] -> empty_subst
    | (e1,e2)::t ->
        let sub = unify e1 e2 in
          compose sub (unify_list (apply_subst_eqlist sub t))


let new_var tv prefix count = 
  fun () -> 
    let newname = count := !count + 1; 
      prefix ^ "var" ^ (string_of_int (!count)) in
      if tv then
	TypeVar(newname)
      else
	AnnVar(newname)

let new_typevar = new_var true "type" (ref(-1))
let new_annvar = new_var false "ann" (ref(-1))
