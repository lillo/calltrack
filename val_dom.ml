open Syntax
open Env

module Env = FunEnv

module type VALUES = 
  sig
    type eval
    type env = (ide, eval) Env.t
    type point_set = point Point_set.t
    type sem_fun = exp -> env -> point_set -> (eval * point_set)

    val make_int : exp  -> eval 
    val make_bool : exp -> eval 

    val int_op : (int -> int -> int) -> eval -> eval -> eval
    val rel_op : (int -> int -> bool) -> eval -> eval -> eval

    val bool_not : eval -> eval
    val bool_op :  sem_fun -> exp -> exp -> env -> point_set -> bool -> (eval * point_set)

    val makefun : sem_fun -> (exp * env * point_set) -> (eval * point_set)
    val makefunrec : sem_fun -> (ide * exp * env * point_set) -> (eval * point_set)
    val applyfun : (eval * eval * point_set) -> (eval * point_set)

    val ifthenelse : sem_fun -> (exp * exp * exp * env * point_set) -> (eval * point_set)

    val eval_printer: Format.formatter -> eval -> unit
  end


module ConcreteDomain : VALUES =
struct
  exception WrongValue of string

  type point_set = point Point_set.t

  type eval = 
      Int of int
    | Bool of bool
    | Funval of efun

  and efun = point * (point_set -> eval -> (eval * point_set))
  and env = (ide, eval) Env.t

  type sem_fun = exp -> env -> point_set -> (eval * point_set)


  let make_int e = 
    match e with
	Eint(n) -> Int(n)
      | _ -> raise (WrongValue("make_int: no integer value"))

  let make_bool e = 
    match e with
	Ebool(b) -> Bool(b)
      | _ -> raise (WrongValue("make_bool: no boolean value"))

  let int_op op v1 v2 = 
    match (v1,v2) with
	(Int(n),Int(n')) -> Int(op n n')
      | _ -> raise (WrongValue("int_op: the argument are not integers"))

  let rel_op op  v1 v2 = 
    match (v1,v2) with
	(Int(n),Int(n')) -> Bool(op n n')
      | _ -> raise (WrongValue("rel_op: the arguments are not integers"))


  let bool_not v1 = 
    match v1 with
	Bool(b) -> Bool(not b)
      | _ -> raise (WrongValue("bool_not: the argument is not a boolean"))
  
  let rec bool_op (sem:sem_fun) e1 e2 r ps db = 
    let (v, ps') = sem e1 r ps in
      match v with
	  Bool(b) as res -> if b = db then sem e2 r ps' else (res, ps')
	| _ -> raise (WrongValue("bool_op: the argument is not a boolean"))

  let makefun (sem: sem_fun)(funv, r, ps) = 
    match funv with
	Fun(point, id, e) -> (Funval(point, fun ps v -> sem e (Env.bind(r,id,v)) ps), ps)
      | _ -> raise (WrongValue("makefun: no lambda-abstraction"))

  let applyfun (funval, v, ps) = 
    match funval with
	Funval(p,f) -> f (Point_set.add_point(p,ps)) v
      | _ -> raise (WrongValue("applyfun: no lambda-abstraction"))

  let makefunrec (sem: sem_fun) (id, funv, r, ps) =
    match funv with
	Fun(point,fp,e) ->
	  let rec functional ff ps' fa = 
	    let r' = Env.bind(Env.bind(r, fp,fa), id, Funval(point,ff)) in
	      sem e r' ps'
	  in
	  let rec fix ps' x = functional fix ps' x in
	    (Funval(point,fix), ps)
      | _ -> raise (WrongValue("makefunrec: no lambda-abstraction"))

  let ifthenelse (sem : sem_fun) (e1, e2, e3, r, ps) =
    let (guard,ps') = sem e1 r ps in
      (match guard with
	   Bool(b) -> if b then sem e2 r ps' else sem e3 r ps'
	 | _ -> raise (WrongValue("ifthenelse: the guard is not a boolean"))
      )

  let eval_printer ppf v = 
    match v with 
	Int(n) -> Format.fprintf ppf "@[ %d - : Integer@]" n
      | Bool(b) -> Format.fprintf ppf "@[ %b - : Boolean@]" b
      | Funval(p,_) -> Format.fprintf ppf "@[ %s - : Funval@]" p
end


module Ann =
struct
  open Tipo 

  type t = (tipo * point) list
      
  let empty_ann = ([] : t)


  let apply_subst_ann (sub:subst) (a:t) = 
    List.map (fun (t,p) -> (apply_subst sub t, p)) a

  let combine_ann (a1:t) (a2:t) = 
    let (_,nl) = List.partition (fun x -> List.mem x a2) a1 in
      nl @ a2

  let combine_ann_list l =
    List.fold_left combine_ann empty_ann l

  let new_ann (sub:subst) (al : t list) = 
    combine_ann_list (List.map (apply_subst_ann sub) al)

  let ann_from_points (avar:tipo) (pl:point Point_set.t) = 
    List.map (fun p -> (avar,p)) (Point_set.to_list pl)

  let extract_point (avar:tipo) (a: t) = 
    Point_set.from_list (snd(List.split (List.filter (fun (var,_) -> var = avar) a)))

  let ann_printer ppf a =
    let bind_printer ppf = function 
	(t,p) -> Format.fprintf ppf"(%a,%s)" tipo_printer t p
    in
    let rec f_aux ppf = function
	[] -> ()
      | h::t -> Format.fprintf ppf ",@[<2>@ %a%a@]" 
	 bind_printer h f_aux t 
    in
      if (List.length a) > 0 then
	Format.fprintf ppf "@[<2>@ [%a%a]@]"
	  bind_printer (List.hd a) f_aux (List.tl a)
      else
	Format.fprintf ppf "@[<2>@ []@]"
end

module AbstractDomain : VALUES = 
struct
  open Tipo
  open Ann
  module Env = FunEnv

  exception No_type of string

  type point_set = point Point_set.t
  type eval = ((tipo * subst) * Ann.t)
  type env = (ide, eval) Env.t
  type sem_fun = exp -> env -> point_set -> (eval * point_set)

  let integer = Symb("Integer",[])
  let boolean = Symb("Boolean",[])
  let tfunction (d, avar, c) = Symb("Function", [d;avar;c])

  let unify_list l s =
    try
      unify_list l
    with Failure(a) -> raise (No_type(a ^ ": " ^ s))

  let fresh_type t = ((t, empty_subst), Ann.empty_ann)
      

  let make_bool e = 
    match e with
	Ebool(_) -> fresh_type boolean
      | _ -> raise (No_type("make_bool: no boolean argument"))

  let make_int e = 
    match e with
	Eint(_) -> fresh_type integer
      | _ -> raise (No_type("make_int: no integer argument"))

  let int_op op (v1:eval) (v2:eval) = 
    let ((t1,sub1),a1) = v1 in
    let ((t2,sub2),a2) = v2 in
    let nsub = unify_list ((t1,integer) :: (t2,integer) :: (sub1 @ sub2))
      "int_op: arguments are not integers"
    in
      ((integer,nsub), new_ann nsub [a1;a2])
	
  let rel_op op (v1:eval) (v2:eval) = 
    let ((t1,sub1),a1) = v1 in
    let ((t2,sub2),a2) = v2 in
    let nsub = unify_list ((t1,integer) :: (t2,integer) :: (sub1 @ sub2))
      "int_op: arguments are not integers"
    in
      ((boolean,nsub), new_ann nsub [a1;a2])

  let bool_not (v:eval) = 
    let ((t,sub),a) = v in
    let nsub = unify_list ((t,boolean) :: sub) 
      "bool_not: argument is not a boolean"
    in
      ((boolean,nsub), apply_subst_ann nsub a)

  let bool_op (sem:sem_fun) (e1:exp) (e2:exp) r ps db =
    let (((t1,sub1),a1),ps1) = sem e1 r ps in
    let (((t2,sub2),a2),ps2) = sem e2 r ps1 in
    let nsub = unify_list ((t1,boolean) :: (t2,boolean) :: (sub1 @ sub2))
      "bool_op: arguments are not booleans"
    in
      (((boolean,nsub), new_ann nsub [a1;a2]),ps2)
	
  let makefun (sem:sem_fun) (funv, r, ps) =
    match funv with
	Fun(point, id, e) ->
	  let tvar = new_typevar () in
	  let avar = new_annvar () in
	  let r' = Env.bind(r, id, fresh_type tvar) in
	  let (((t,sub),a),ps') = sem e r' (Point_set.add_point(point, ps)) in
	  let a1 = ann_from_points avar ps' in
	    (((tfunction(apply_subst sub tvar, avar, t),sub), 
	     new_ann sub [a;a1]),ps)
      | _ -> raise (No_type("makefun: no functional object"))

  let makefunrec (sem: sem_fun) (id, funv, r, ps) = 
    match funv with
	Fun(_) ->
	  let tvar' = new_typevar () in
	  let tvar'' = new_typevar () in
	  let avar = new_annvar () in
	  let tf = tfunction(tvar',avar,tvar'') in
	  let r' = Env.bind(r, id, fresh_type tf) in
	    makefun sem (funv, r', ps)
      | _ -> raise (No_type("makefunrec: no functional object"))

  let applyfun (v1, v2, ps) = 
    let ((tf,sub1),a1) = v1 in
    let ((t, sub2),a2) = v2 in
    let tvar = new_typevar () in
    let avar = new_annvar () in
    let nsub = unify_list ((tf, (tfunction(t,avar,tvar))) :: (sub1@sub2))
      "applyfun: no unification"
    in
    let ps1 = extract_point (apply_subst nsub avar) (apply_subst_ann nsub a1) in
      (((apply_subst nsub tvar, nsub), new_ann nsub [a1;a2]), Point_set.combine_set(ps,ps1))
	
  let ifthenelse (sem:sem_fun) (e1,e2,e3,r,ps) = 
    let (((tg,sub1),a1), ps1) = sem e1 r ps in
    let ns = unify_list ((tg,boolean)::sub1)
      "ifthenelse: no boolean guard"
    in
    let (((tt,sub2),a2), ps2) = sem e2 r ps1 in
    let (((te,sub3),a3), ps3) = sem e3 r ps1 in
    let nsub = unify_list ((tt,te) :: (ns @ sub1 @ sub3))
      "ifthenelse: then branch and else branch have different types"
    in
      ((((apply_subst nsub tt),nsub), new_ann nsub [a1;a2;a3]),ps3)

	
  let eval_printer ppf v = 
    let ((t,_),a) = v in
      Format.fprintf ppf "@[<2>type - :@ %a%a@]"
	tipo_printer  t ann_printer  a
      
end
