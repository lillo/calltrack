OCAMLC=ocamlc
OCAMLMKTOP=ocamlmktop

CALLTRACK=calltrack
OBJECTS=syntax.cmo point_set.cmo env.cmo tipo.cmo val_dom.cmo sem.cmo example.cmo

.PHONY: clean dist-clean

all: calltrack

calltrack: objs
	$(OCAMLMKTOP) $(OBJECTS) -o $(CALLTRACK)

objs: $(OBJECTS)

clean:
	rm -f *.cm[iox] *~

dist-clean: clean
	rm -f $(CALLTRACK)

.SUFFIXES: .ml .cmo

%.cmo : %.ml
	$(OCAMLC) -c $<
