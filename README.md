# README #

An abstract semantics implementing a type and effect system for tracking function call in a small fragment of the ML language.

This project is a proof of concept implementing the abstract semantics described in 

- [**A Reconstruction of a Types-and-Effects Analysis by Abstract Interpretation**](http://ictcs.di.unimi.it/papers/paper_31.pdf), L. Galletta, ICTCS 2012  

- **An Abstract Interpretation Framework for Type and Effect Systems**, L. Galletta, Fundamenta Informaticae 134 (3-4), 2014. 


## Requirements to build the project #

The project requires 

- [OCaml](http://www.ocaml.org/) compiler

- Make


## Building the project #

To build type in the command line:

```
 $ make all

```

## Running #

The command:

```
 $ calltrack

```

starts an OCaml toplevel with pre-loaded all the libraries for the analysis.

### Examples
The file examples.ml contains some examples of programs in abstract syntax tree.


## Content of the repository #

- README.md                                  this file