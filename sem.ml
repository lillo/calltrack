open Syntax
open Env
open Val_dom

module Env = FunEnv

module Semantics (Values : VALUES) = 
struct
  open Values
  open Env

  let rec sem (e:exp) (r: Values.env) (ps: point Point_set.t) = 
    match e with
      Eint(num) -> (make_int e, ps)
    | Ebool(b) -> (make_bool e, ps)
    | Ide(id) -> (apply_env(r, id), ps)
    | Fun(point, id, e1) -> makefun sem (e, r, ps)
    | Let(id, e1, e2) ->
	let (v, ps') = sem e1 r ps in
	let r' = Env.bind(r, id, v) in
	  sem e2 r' ps'
    | IfThenElse(e1,e2,e3) ->
	ifthenelse sem (e1, e2, e3, r, ps)
    | Apply(e1,e2) -> 
	let (fn,ps') = sem e1 r ps in
	let (ap,ps'') = sem e2 r ps' in 
	  applyfun (fn, ap, ps'')
    | Rec(id,e) -> makefunrec sem (id, e, r, ps)
    | Plus(e1,e2) -> 
	let (v1, ps') = sem e1 r ps in
	let (v2, ps'') = sem e2 r ps' in
	  (int_op (+) v1 v2, ps'')
    | Minus(e1,e2) -> 
	let (v1, ps') = sem e1 r ps in
	let (v2, ps'') = sem e2 r ps' in
	  (int_op (-) v1 v2, ps'')
    | Times(e1,e2) -> 
	let (v1, ps') = sem e1 r ps in
	let (v2, ps'') = sem e2 r ps' in
	  (int_op ( * ) v1 v2, ps'')
    | And(e1,e2) -> bool_op sem e1 e2 r ps true
    | Or(e1,e2) -> bool_op sem e1 e2 r ps false
    | Not(e) -> 
	let (v,ps') = sem e r ps in
	  (bool_not v, ps')
    | Iszero(e) ->
	let (v, ps') = sem e r ps in
	  (rel_op (=) v (make_int (Eint(0))), ps')
    | Less(e1,e2) -> 
	let (v1, ps') = sem e1 r ps in
	let (v2, ps'') = sem e2 r ps' in
	  (rel_op (<) v1 v2, ps'')
    | Greater(e1,e2) -> 
	let (v1, ps') = sem e1 r ps in
	let (v2, ps'') = sem e2 r ps' in
	  (rel_op (>) v1 v2, ps'')

  let ps_to_string (ps: point Point_set.t) =
    let bf = Buffer.create 16 in
    let pl = Point_set.to_list ps in
    let rec f_aux = function 
	[] -> ()
      | h::t -> Buffer.add_string bf (", " ^ h);
	  f_aux t
    in
      if (List.length pl) > 0 then
	(Buffer.add_string bf (List.hd pl);
	 f_aux (List.tl pl))
      ;
      "{" ^ (Buffer.contents bf) ^ "}"

  let point_set_printer ppf ps = 
    Format.fprintf ppf "@[%s@]" (ps_to_string ps)

  let printer ppf ((value, point_set): Values.eval * point Point_set.t) =
    Format.fprintf ppf "@[<2>(%a@ &@ %a)@]" eval_printer value point_set_printer point_set
end


module ConcreteSemantics = Semantics(ConcreteDomain)
module AbstractSemantics = Semantics(AbstractDomain)

let csem = ConcreteSemantics.sem
let cprinter = ConcreteSemantics.printer

let asem = AbstractSemantics.sem
let aprinter = AbstractSemantics.printer



