type ide = string

type point = string

type exp = 
    Eint of int
  | Ebool of bool
  | Ide of ide
  | Fun of point * ide * exp
  | Let of ide * exp * exp
  | IfThenElse of exp * exp * exp
  | Apply of exp * exp
  | Rec of ide * exp
  | Plus of exp * exp
  | Minus of exp * exp
  | Times of exp * exp
  | And of exp * exp
  | Or of exp * exp
  | Not of exp
  | Iszero of exp
  | Less of exp * exp
  | Greater of exp * exp
    
